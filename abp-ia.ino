//referencia
//https://github.com/alvesoaj/eFLL/blob/master/examples/arduino_simple_sample/arduino_simple_sample.ino

#include <Fuzzy.h>

#define pot1 A0
#define pot2 A2
#define ledR 9
#define ledG 8
#define ledB 7

Fuzzy *fuzzy = new Fuzzy();

float humidity = 0.0;
float temperature = 0.0;

void  setup() 
{
  Serial.begin(9600);
  pinMode(ledR,OUTPUT);
  pinMode(ledG,OUTPUT);
  pinMode(ledB,OUTPUT);
  pinMode(pot1,INPUT);
  pinMode(pot2,INPUT);

  //Inputs do fuzzy
  FuzzyInput *humidityInput    = new FuzzyInput(1);
  FuzzyInput *temperatureInput = new FuzzyInput(2);

  //Outputs do fuzzy
  FuzzyOutput *growingTime = new FuzzyOutput(1);

  //Regras Temperatura
  FuzzySet *lowTemperature    = new FuzzySet(10, 15, 15, 20);
  FuzzySet *mediumTemperature = new FuzzySet(15, 20, 20, 25);
  FuzzySet *highTemperature   = new FuzzySet(20, 25, 25, 30);

  //Regras umidade
  FuzzySet *lowHumidity    = new FuzzySet(30, 40, 40, 50);
  FuzzySet *mediumHumidity = new FuzzySet(40, 50, 50, 60);
  FuzzySet *highHumidity   = new FuzzySet(50, 60, 60, 70);

  //Regras tempo de crescimento
  FuzzySet *shortTime  = new FuzzySet(10, 12, 12, 15);
  FuzzySet *mediumTime = new FuzzySet(12, 15, 15, 18);
  FuzzySet *longTime   = new FuzzySet(15, 18, 18, 20);

  //Adicionando as regras nos inputs
  humidityInput->addFuzzySet(lowHumidity);
  humidityInput->addFuzzySet(mediumHumidity);
  humidityInput->addFuzzySet(highHumidity);

  temperatureInput->addFuzzySet(lowTemperature);
  temperatureInput->addFuzzySet(mediumTemperature);
  temperatureInput->addFuzzySet(highTemperature);

  growingTime->addFuzzySet(shortTime);
  growingTime->addFuzzySet(mediumTime);
  growingTime->addFuzzySet(longTime);

  //Adicionando os inputs e output
  fuzzy->addFuzzyInput(humidityInput);
  fuzzy->addFuzzyInput(temperatureInput);
  fuzzy->addFuzzyOutput(growingTime);

  //Criando as regras
  //Regra 1 - Se a temperatura é baixa E a umidade é média, então o tempo de crescimento é curto.
  FuzzyRuleAntecedent *lowTemperatureMediumHumidity = new FuzzyRuleAntecedent();
  lowTemperatureMediumHumidity->joinWithAND(lowTemperature, mediumHumidity);
  FuzzyRuleConsequent *thenShortTime = new FuzzyRuleConsequent();
  thenShortTime->addOutput(shortTime);
  FuzzyRule *fuzzyRule1 = new FuzzyRule(1, lowTemperatureMediumHumidity, thenShortTime);
  fuzzy->addFuzzyRule(fuzzyRule1);

  //Regra 2 - Se a temperatura é média E a umidade é alta, então o tempo de crescimento é médio.
  FuzzyRuleAntecedent *mediumTemperatureHighHumidity = new FuzzyRuleAntecedent();
  mediumTemperatureHighHumidity->joinWithAND(mediumTemperature, highHumidity);
  FuzzyRuleConsequent *thenMediumTime = new FuzzyRuleConsequent();
  thenMediumTime->addOutput(mediumTime);
  FuzzyRule *fuzzyRule2 = new FuzzyRule(2, mediumTemperatureHighHumidity, thenShortTime);
  fuzzy->addFuzzyRule(fuzzyRule2);
  
  //Regra 3 - Se a temperatura é alta E a umidade é alta, então o tempo de crescimento é longo.
  FuzzyRuleAntecedent *highTemperatureHighHumidity = new FuzzyRuleAntecedent();
  highTemperatureHighHumidity->joinWithAND(highTemperature, highHumidity);
  FuzzyRuleConsequent *thenLongTime = new FuzzyRuleConsequent();
  thenLongTime->addOutput(longTime);
  FuzzyRule *fuzzyRule3 = new FuzzyRule(3, highTemperatureHighHumidity, thenLongTime);
  fuzzy->addFuzzyRule(fuzzyRule3);

  //Regra 4 - Se a temperatura é baixa E a umidade é baixa, então o tempo de crescimento é curto.
  FuzzyRuleAntecedent *lowTemperaturelowHumidity = new FuzzyRuleAntecedent();
  lowTemperaturelowHumidity->joinWithAND(lowTemperature, lowHumidity);
  FuzzyRuleConsequent *thenShortTime1 = new FuzzyRuleConsequent();
  thenShortTime1->addOutput(shortTime);
  FuzzyRule *fuzzyRule4 = new FuzzyRule(4, lowTemperaturelowHumidity, thenShortTime1);
  fuzzy->addFuzzyRule(fuzzyRule4);
  
  //Regra 5 - Se a temperatura é alta E a umidade é media, então o tempo de crescimento é longo.
  FuzzyRuleAntecedent *highTemperatureMediumHumidity = new FuzzyRuleAntecedent();
  highTemperatureMediumHumidity->joinWithAND(highTemperature, mediumHumidity);
  FuzzyRuleConsequent *thenLongTime1 = new FuzzyRuleConsequent();
  thenLongTime1->addOutput(longTime);
  FuzzyRule *fuzzyRule5 = new FuzzyRule(5, highTemperatureMediumHumidity, thenLongTime1);
  fuzzy->addFuzzyRule(fuzzyRule5);

  //Regra 6 - Se a temperatura é media E a umidade é media, então o tempo de crescimento é medio.
  FuzzyRuleAntecedent *mediumTemperatureMediumHumidity = new FuzzyRuleAntecedent();
  mediumTemperatureMediumHumidity->joinWithAND(mediumTemperature, mediumHumidity);
  FuzzyRuleConsequent *thenMediumTime1 = new FuzzyRuleConsequent();
  thenMediumTime1->addOutput(mediumTime);
  FuzzyRule *fuzzyRule6 = new FuzzyRule(6, mediumTemperatureMediumHumidity, thenMediumTime1);
  fuzzy->addFuzzyRule(fuzzyRule6);
}

void loop()
{ 
  long humidityRead = analogRead(pot1);
  long temperatureRead = analogRead(pot2);
  
  long mappedHumidity = map(humidityRead, 0, 1023, 31, 69);
  long mappedTemperature = map(temperatureRead, 0, 1023, 11, 29);
  
  Serial.println("\n\n\nEntrada: ");
  Serial.print("\t\t\tUmidade: ");
  Serial.print(mappedHumidity);
  Serial.print("\t\t\tTemperatura: ");
  Serial.println(mappedTemperature);

  fuzzy->setInput(1, mappedHumidity);
  fuzzy->setInput(2, mappedTemperature);
  fuzzy->fuzzify();
  float output = fuzzy->defuzzify(1);
  
  Serial.println("Resultado: ");
  Serial.print("\t\t\tTempo de crescimento: ");
  Serial.println(output);

  if(output >= 10 && output <= 13){
    digitalWrite(ledR, LOW);
    digitalWrite(ledG, HIGH);
    digitalWrite(ledB, LOW);
  }
  if(output > 13 && output <= 15){
    digitalWrite(ledR, LOW);
    digitalWrite(ledG, LOW);
    digitalWrite(ledB, HIGH);
  }
  if(output > 15){
    digitalWrite(ledR, HIGH);
    digitalWrite(ledG, LOW);
    digitalWrite(ledB, LOW);  
  }
  
  delay(1000);
}
